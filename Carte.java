import java.util.Scanner;
public class Carte {
    public static void main(String[] args) {
        Bookstore c1 = new Bookstore("Crima si pedeapsa", "Dostoievski", 1866, Tip.ROMAN);
        Scanner ip = new Scanner(System.in);
        System.out.println("Introduceti tipul cartii dupa numar: ");
        int id = ip.nextInt();
        Tip tip = Tip.values()[id];
        System.out.println("A fost ales " + tip);
        if (c1.tipulCartii == tip)
            System.out.println("Cartea " + c1.nume + " corespunde tipului ales.");
    }
}
